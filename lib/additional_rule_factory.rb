# frozen_string_literal: true
#
class AdditionalRuleFactory
  def self.create_rules
    rules = {}

    all_keys = {
      sensiseeds: { domains: %w[www.sensiseeds.com sensiseeds.com], parameter: "a_aid", remove_slash: true },
      weedseedshop: { domains: %w[www.weedseedshop.com weedseedshop.com], parameter: "a_aid", remove_slash: true },
      growshop24: { domains: %w[www.grow-shop24.de grow-shop24.de], parameter: "ref", remove_slash: true },
      proemit: { domains: %w[www.pro-emit.de pro-emit.de shop.pro-emit.de], parameter: "sPartner", remove_slash: true },
      bloomtech: { domains: %w[www.bloomtech.de bloomtech.de], parameter: "aref", remove_slash: true },
      urbanchili: { domains: %w[www.urbanchili.eu urbanchili.eu], parameter: "ref", add_parameter: { campaign: "ca-forum" } },
      minigrowbox: { domains: %w[www.minigrowbox.com minigrowbox.com], parameter: "wpam_id" },
      undrcovrlab: { domains: %w[www.undrcovrlab.com undrcovrlab.com], parameter: "ref", add_slash: true },
      seedsman: { domains: %w[www.seedsman.com seedsman.com], parameter: "a_aid" },
      cannapot: { domains: %w[www.cannapot.com cannapot.com], parameter: "id", append_uri: "track.php" },
      vaposhop: { domains: %w[www.vaposhop.de vaposhop.de], parameter: "a" },
      drehmohment: { domains: %w[www.drehmoment-headshop.de drehmoment-headshop.de], parameter: "ref" },
      dutchpassion: { domains: %w[www.dutch-passion dutch-passion.com], parameter: "afmc" },
      nordicoil: { domains: %w[nordicoil.de], parameter: "a_aid",
                  add_parameter: {
        utm_source: "pap",
        utm_medium: "affiliate",
        utm_campaign: "logo",
        utm_term: "5c5a90d504e20",
        a_bid: "59323bd0",
      } },
    }

    # TODO:
    # add_parameter
    # append_uri
    # remove_slash
    # add_slash

    all_keys.each do |key, data|
      domains = data[:domains]
      parameter = data[:parameter]
      code = SiteSetting.get("affiliate_#{key}")

      rule = lambda do |url, uri|
        if code.present?
          uri.path.chomp!("/") if data[:remove_slash] || data[:add_slash]
          uri.path.concat("/") if data[:add_slash]

          query_array = [[parameter, code]]

          if data[:add_parameter]
            query_array += data[:add_parameter].to_a
          end

          if data[:append_uri]
            uri.path.chomp!(data[:append_uri])
            uri.path.concat(data[:append_uri])
          end

          uri.query = URI.encode_www_form(query_array)

          uri.to_s
        else
          url
        end
      end

      domains.each do |domain|
        rules[domain] = rule
      end
    end

    rules
  end
end
