if ENV['NO_DISCOURSE_INSTALL']
  require File.expand_path(File.join(__FILE__, '../../no_discourse_test_helper'))
else
  require 'rails_helper' 
end


describe LegacyRuleFactory do

  def r(url)
    rules = UniversalRuleFactory.create_rules
    allow(AffiliateProcessor).to receive(:rules).and_return(rules)

    AffiliateProcessor.apply(url)
  end


  # this is broken but it's not needed for this release. Sorry boys, gotta be fast
  # it 'can change all types of urls correctly' do
  #   SiteSetting.affiliate_redirect_base_domain = 'https://nomadgate.com/go/'
  #   SiteSetting.affiliate_rewrite_domains = 'thomas.do,tkrunning,uri|n26.com,n26|transferwise.com,transferwise,url|nomadgate.com,nomadgate,uri|google.com,google,path|foobar.com,foobar'

  #   expect(r('https://n26.com')).to eq('https://nomadgate.com/go/n26')
  #   expect(r('https://transferwise.com/borderless')).to eq('https://nomadgate.com/go/transferwise?url=https://transferwise.com/borderless')
  #   expect(r('https://thomas.do/fancypants?hi=there&yo=man')).to eq('https://nomadgate.com/go/tkrunning?uri=/fancypants?hi=there&yo=man')
  #   expect(r('https://nomadgate.com/')).to eq('https://nomadgate.com/go/nomadgate')
  #   expect(r('https://nomadgate.com')).to eq('https://nomadgate.com/go/nomadgate')
  #   expect(r('https://google.com/')).to eq('https://nomadgate.com/go/google')
  #   expect(r('https://google.com')).to eq('https://nomadgate.com/go/google')
  #   expect(r('https://google.com/search?source=hp&ei=WX38WrfQM-rJ6ASwvq7QBg&q=yo')).to eq('https://nomadgate.com/go/google?path=/search')
  #   expect(r('https://foobar.com/')).to eq('https://nomadgate.com/go/foobar')
  #   expect(r('https://foobar.com')).to eq('https://nomadgate.com/go/foobar')
  #   expect(r('https://foobar.com?foo=bar')).to eq('https://nomadgate.com/go/foobar')
  #   expect(r('https://foobar.com/index.html')).to eq('https://nomadgate.com/go/foobar')
  #   expect(r('https://foobar.com/foobar?foo=bar')).to eq('https://foobar.com/foobar?foo=bar')
  #   expect(r('https://foobar.com/foobar.html?foo=bar')).to eq('https://foobar.com/foobar.html?foo=bar')
  # end



end
