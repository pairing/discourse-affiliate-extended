# frozen_string_literal: true
#
class UniversalRuleFactory
  
  def self.create_rules
    rules = {}

    #universal
    domain_rules = SiteSetting.affiliate_rewrite_domains
    domain_rules.split("|").each do |domain_rule|
      domain_name = domain_rule.split(",")[0]
      rule = lambda do |url, uri|
        slug = domain_rule.split(",")[1]
        should_rewrite = true
        if domain_rule.split(",")[2] == "url"
          slug = slug + "?url=" + url
        elsif domain_rule.split(",")[2] == "uri"
          if URI(url).request_uri != "/"
            slug = slug + "?uri=" + URI(url).request_uri
          end
        elsif domain_rule.split(",")[2] == "path"
          if URI(url).path != "/" && URI(url).path != ""
            slug = slug + "?path=" + URI(url).path
          end
        else
          if URI(url).path != "/" && URI(url).path != "" && URI(url).path != "/index.html" && URI(url).path != "/index.htm"
            should_rewrite = false
          end
        end
        base = SiteSetting.affiliate_redirect_base_domain
        if base.present? && should_rewrite == true
          uri = base + slug
          uri.to_s
        else
          url
        end
      end

      ap "add rule: #{domain_name}"

      rules[domain_name] = rule
    end
  end

end
