
require 'awesome_print'

# gem install activesupport
require 'active_support/all'


plugin_dir = File.join(File.expand_path(File.dirname(__FILE__)), '..')

require File.join(plugin_dir, '/lib/affiliate_processor')
require File.join(plugin_dir, '/lib/amazon_rule_factory')
require File.join(plugin_dir, '/lib/additional_rule_factory')


class SiteSetting

  @methods = {}

  def self.method_missing(m, *args, &block)
    if m[-1] == '='
      @methods[m[0..-2].to_sym] = args[0]
    else
      @methods[m.to_sym]
    end
  end

  def self.get(name)
    @methods[name.to_sym]
  end


end
