if ENV['NO_DISCOURSE_INSTALL']
  require File.expand_path(File.join(__FILE__, '../../no_discourse_test_helper'))
else
  require 'rails_helper' 
end


describe AmazonRuleFactory do

  def r(url)
    rules = AmazonRuleFactory.create_rules
    allow(AffiliateProcessor).to receive(:rules).and_return(rules)

    AffiliateProcessor.apply(url)
  end

  # In case you load rspec without a discourse install, this should be working:
  it 'loads active support' do
    expect('foo'.present?).to eq(true)
  end

  it 'can apply affiliate code correctly' do
    SiteSetting.affiliate_amazon_com = 'sams-shop'
    SiteSetting.affiliate_amazon_com_au = 'au-sams-shop'

    expect(r('https://www.amazon.com')).to eq('https://www.amazon.com?tag=sams-shop')

    expect(r('http://www.amazon.com/some_product?xyz=1')).to eq('http://www.amazon.com/some_product?tag=sams-shop')
    expect(r('https://www.amazon.com/some_product?xyz=1')).to eq('https://www.amazon.com/some_product?tag=sams-shop')
    expect(r('https://amzn.com/some_product?xyz=1')).to eq('https://amzn.com/some_product?tag=sams-shop')
    expect(r('https://smile.amazon.com/some_product?xyz=1')).to eq('https://smile.amazon.com/some_product?tag=sams-shop')
    expect(r('https://www.amazon.com.au/some_product?xyz=1')).to eq('https://www.amazon.com.au/some_product?tag=au-sams-shop')

    # keep node (BrowseNodeSearch) query parameter
    expect(r('https://www.amazon.com/b?ie=UTF8&node=13548845011')).to eq('https://www.amazon.com/b?tag=sams-shop&node=13548845011')
  end


end
