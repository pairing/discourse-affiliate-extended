if ENV['NO_DISCOURSE_INSTALL']
  require File.expand_path(File.join(__FILE__, '../../no_discourse_test_helper'))
else
  require 'rails_helper'
end


describe AffiliateProcessor do

  def r(url)
    AffiliateProcessor.apply(url)
  end

  # In case you load rspec without a discourse install, you need 2 things to mock
  # SiteSetting class. see no_discourse_test_helper.rb
  it 'saves the correct site settings' do
    SiteSetting.foo = 'bar'
    expect(SiteSetting.foo).to eq('bar')
  end

  # active support loaded. also see: no_discourse_test_helper.rb
  it 'loads active support' do
    expect('foo'.present?).to eq(true)
  end

  it 'loads only certain rules' do
    expect(AmazonRuleFactory).to receive(:create_rules).and_call_original
    expect(AdditionalRuleFactory).to receive(:create_rules).and_call_original

    expect(LegacyRuleFactory).not_to receive(:create_rules).and_call_original
    expect(UniversalRuleFactory).not_to receive(:create_rules).and_call_original

    # just load it once
    AffiliateProcessor.apply('https://example.org')
  end

  unless ENV['NO_DISCOURSE_INSTALL']
    it 'can apply codes to post in post processor' do
      SiteSetting.queue_jobs = false
      SiteSetting.affiliate_amazon_com = 'sams-shop'

      post = create_post(raw: 'this is an www.amazon.com/link?testing yay')
      post.reload

      expect(post.cooked.scan('sams-shop').length).to eq(1)
    end
  end

end
