if ENV['NO_DISCOURSE_INSTALL']
  require File.expand_path(File.join(__FILE__, '../../no_discourse_test_helper'))
else
  require 'rails_helper' 
end


describe LegacyRuleFactory do

  def r(url)
    rules = LegacyRuleFactory.create_rules
    allow(AffiliateProcessor).to receive(:rules).and_return(rules)

    AffiliateProcessor.apply(url)
  end


  it 'can apply affiliate code to ldlc' do
    SiteSetting.affiliate_ldlc_com = 'samsshop'

    expect(r('http://www.ldlc.com/some_product?xyz=1')).to eq('http://www.ldlc.com/some_product?xyz=1#samsshop')
    expect(r('https://ldlc.com/some_product?xyz=1')).to eq('https://ldlc.com/some_product?xyz=1#samsshop')
  end

  it 'can apply affiliate code to flystein' do
    SiteSetting.affiliate_flystein = 'samsshop'

    expect(r('https://flystein.com/some_product')).to eq('https://flystein.com/some_product?af=samsshop')
  end

  it 'can apply affiliate code to grow-shop24.de' do
    SiteSetting.affiliate_growshop24 = 'Jm5siqwQTjB0lgZjkb07'

    expect(r('https://www.grow-shop24.de/homebox')).to eq('https://www.grow-shop24.de/homebox?ref=Jm5siqwQTjB0lgZjkb07')
    expect(r('https://www.grow-shop24.de/homebox/')).to eq('https://www.grow-shop24.de/homebox?ref=Jm5siqwQTjB0lgZjkb07')
  end

  it 'can apply affiliate code to pro-emit.de' do
    SiteSetting.affiliate_proemit = 'cannabisanbauen'

    expect(r('https://www.pro-emit.de/')).to eq('https://www.pro-emit.de?sPartner=cannabisanbauen')
    expect(r('https://shop.pro-emit.de/')).to eq('https://shop.pro-emit.de?sPartner=cannabisanbauen')
    expect(r('https://shop.pro-emit.de/pro-emit/127/sunflow-4er-bundle.html/')).to eq('https://shop.pro-emit.de/pro-emit/127/sunflow-4er-bundle.html?sPartner=cannabisanbauen')
  end



end
