# frozen_string_literal: true
#

require File.join(File.dirname(__FILE__), 'amazon_rule_factory')
require File.join(File.dirname(__FILE__), 'legacy_rule_factory')
require File.join(File.dirname(__FILE__), 'universal_rule_factory')
require File.join(File.dirname(__FILE__), 'additional_rule_factory')

class AffiliateProcessor

  def self.rules
    return @rules if @rules
    
    @rules = {}

    @rules.merge! AmazonRuleFactory.create_rules
    @rules.merge! AdditionalRuleFactory.create_rules

    # @rules.merge! LegacyRuleFactory.create_rules
    # @rules.merge! UniversalRuleFactory.create_rules
    
    @rules = rules
  end

  def self.apply(url)
    uri = URI.parse(url)

    if uri.scheme == 'http' || uri.scheme == 'https'
      rule = rules[uri.host]
      return rule.call(url, uri) if rule
    end

    url
  rescue
    url
  end

end
