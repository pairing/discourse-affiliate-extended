## discourse-affiliate

The Discourse Affiliate plugin can help you monetize your Discourse forum. When users create links and other users click through those links and buy products from Amazon, they earn referral fees.

Official documentation at https://meta.discourse.org/t/discourse-affiliate-plugin/101937


## The extension

We add more rules (aka rule factories) to the mix. See lib folder, it now has multiple rule sets that you can switch on/off (by hardcoding)

If you want to run this plugin without a full discourse install, just do this:

```
gem install awesome_print
gem install activesupport
```

awesome print and the `ap` command is not crucial.

Then run `NO_DISCOURSE_INSTALL=1 rspec spec/lib/`




## Issues

If you have issues or suggestions for the plugin, please bring them up on [Discourse Meta](https://meta.discourse.org).

## License

MIT
