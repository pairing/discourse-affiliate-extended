if ENV['NO_DISCOURSE_INSTALL']
  require File.expand_path(File.join(__FILE__, '../../no_discourse_test_helper'))
else
  require 'rails_helper' 
end


describe LegacyRuleFactory do

  def r(url)
    rules = AdditionalRuleFactory.create_rules
    allow(AffiliateProcessor).to receive(:rules).and_return(rules)

    AffiliateProcessor.apply(url)
  end
  
  
  # Sensiseeds.com:
  # append ?a_aid=17331B18
  # Any trailing slash at the end of the URL (and before the affiliate parameter) has to be removed 
  
  # normal: https://sensiseeds.com/de/feminisierte-samen/sensi-seeds/northern-lights-autoflowering
  # affiliate: https://sensiseeds.com/de/feminisierte-samen/sensi-seeds/northern-lights-autoflowering?a_aid=17331B18
  it 'changes correctly for Sensiseeds' do
    SiteSetting.affiliate_sensiseeds = '471166642'

    old_url = 'https://sensiseeds.com/de/feminisierte-samen/sensi-seeds/northern-lights-autoflowering/'
    new_url = 'https://sensiseeds.com/de/feminisierte-samen/sensi-seeds/northern-lights-autoflowering?a_aid=471166642'
    expect(r(old_url)).to eq(new_url)

    old_url = 'https://sensiseeds.com/de/feminisierte-samen/sensi-seeds/northern-lights-autoflowering?a_aid=foooo'
    new_url = 'https://sensiseeds.com/de/feminisierte-samen/sensi-seeds/northern-lights-autoflowering?a_aid=471166642'
    expect(r(old_url)).to eq(new_url)
  end


  # Weedseedshop.com:
  # append ?a_aid=17331B18
  # Any trailing slash at the end of the URL (and before the affiliate parameter) has to be removed 

  # normal: https://weedseedshop.com/de/feminisierte-samen/critical-kush-feminisiert
  # affiliate: https://weedseedshop.com/de/feminisierte-samen/critical-kush-feminisiert?a_aid=17331B18

  it 'changes correctly for weedseedshop' do
    SiteSetting.affiliate_weedseedshop = '471166642'

    old_url = 'https://weedseedshop.com/de/feminisierte-samen/critical-kush-feminisiert/'
    new_url = 'https://weedseedshop.com/de/feminisierte-samen/critical-kush-feminisiert?a_aid=471166642'
    expect(r(old_url)).to eq(new_url)
  end

  # grow-shop24.de:
  # same parameter as in current version (https://github.com/reflectively/discourse-affiliate-caforum/invitations)
  it 'can apply affiliate code to grow-shop24.de' do
    SiteSetting.affiliate_growshop24 = 'fooFoofooFoo'

    expect(r('https://www.grow-shop24.de/homebox')).to eq('https://www.grow-shop24.de/homebox?ref=fooFoofooFoo')
    expect(r('https://www.grow-shop24.de/homebox/')).to eq('https://www.grow-shop24.de/homebox?ref=fooFoofooFoo')
  end

  # pro-emit.de:
  # same parameter as in current version (https://github.com/reflectively/discourse-affiliate-caforum/invitations)
  it 'can apply affiliate code to pro-emit.de' do
    SiteSetting.affiliate_proemit = 'fooFoofooFoo'

    expect(r('https://www.pro-emit.de/')).to eq('https://www.pro-emit.de?sPartner=fooFoofooFoo')
    expect(r('https://shop.pro-emit.de/')).to eq('https://shop.pro-emit.de?sPartner=fooFoofooFoo')
    expect(r('https://shop.pro-emit.de/pro-emit/127/sunflow-4er-bundle.html/')).to eq('https://shop.pro-emit.de/pro-emit/127/sunflow-4er-bundle.html?sPartner=fooFoofooFoo')
  end

  # Bloomtech.de:
  # Any trailing slash at the end of the URL (and before the affiliate parameter) has to be removed 

  # normal: https://bloomtech.de/Eazy-Plug-rund-104er-Tray
  # affiliate: https://bloomtech.de/Eazy-Plug-rund-104er-Tray?aref=jg
  it 'changes correctly for bloomtech' do
    SiteSetting.affiliate_bloomtech = '471166642'

    old_url = 'https://bloomtech.de/Eazy-Plug-rund-104er-Tray/'
    new_url = 'https://bloomtech.de/Eazy-Plug-rund-104er-Tray?aref=471166642'
    expect(r(old_url)).to eq(new_url)

    old_url = 'https://bloomtech.de'
    new_url = 'https://bloomtech.de?aref=471166642'
    expect(r(old_url)).to eq(new_url)

    old_url = 'https://bloomtech.de?aref=fooo'
    new_url = 'https://bloomtech.de?aref=471166642'
    expect(r(old_url)).to eq(new_url)
  end


  # urbanchili.eu:
  # append ?ref=2&campaign=ca-forum

  # normal: https://urbanchili.eu/shop/
  # affiliate: https://urbanchili.eu/shop/?ref=2&campaign=ca-forum
  it 'changes correctly for urbanchili' do
    SiteSetting.affiliate_urbanchili = '2'

    old_url = 'https://urbanchili.eu/shop/'
    new_url = 'https://urbanchili.eu/shop/?ref=2&campaign=ca-forum'
    expect(r(old_url)).to eq(new_url)
  end


  # minigrowbox.com:
  # append ?wpam_id=4

  # normal: https://minigrowbox.com/shop/minigrowbox/
  # affiliate: https://minigrowbox.com/shop/minigrowbox/?wpam_id=4
  it 'changes correctly for minigrowbox' do
    SiteSetting.affiliate_minigrowbox = '4'

    old_url = 'https://minigrowbox.com/shop/minigrowbox/'
    new_url = 'https://minigrowbox.com/shop/minigrowbox/?wpam_id=4'
    expect(r(old_url)).to eq(new_url)
  end


  # undrcovrlab.com:
  # append ?ref=jrgong
  # IMPORTANT: trailing slash before the affiliate parameter necessary!

  # normal: https://undrcovrlab.com/de/produkt/undrcovrlab-s-growbox-75x45x165cm/
  # affiliate: https://undrcovrlab.com/de/produkt/undrcovrlab-s-growbox-75x45x165cm/?ref=jrgong
  it 'changes correctly for undrcovrlab' do
    SiteSetting.affiliate_undrcovrlab = 'jrgong'

    old_url = 'https://undrcovrlab.com/de/produkt/undrcovrlab-s-growbox-75x45x165cm'
    new_url = 'https://undrcovrlab.com/de/produkt/undrcovrlab-s-growbox-75x45x165cm/?ref=jrgong'
    expect(r(old_url)).to eq(new_url)
  end


  # seedsman.com:
  # append ?a_aid=58da4be961aa1

  # normal: https://www.seedsman.com/de/hanfsamen-breeder/royal-queen-seeds/
  # affiliate: https://www.seedsman.com/de/hanfsamen-breeder/royal-queen-seeds/?a_aid=58da4be961aa1 
  it 'changes correctly for seedsman' do
    SiteSetting.affiliate_seedsman = '471166642'

    old_url = 'https://www.seedsman.com/de/hanfsamen-breeder/royal-queen-seeds/'
    new_url = 'https://www.seedsman.com/de/hanfsamen-breeder/royal-queen-seeds/?a_aid=471166642'
    expect(r(old_url)).to eq(new_url)
  end

  # cannapot:

  # normal: https://www.cannapot.com/shop/hanfsamen/neue-samen/
  # affiliate: https://www.cannapot.com/shop/hanfsamen/neue-samen/track.php?id=juniorgong
  it 'changes correctly for cannapot' do
    SiteSetting.affiliate_cannapot = 'foooo'

    old_url = 'https://www.cannapot.com/shop/hanfsamen/neue-samen/'
    new_url = 'https://www.cannapot.com/shop/hanfsamen/neue-samen/track.php?id=foooo'
    expect(r(old_url)).to eq(new_url)

    old_url = 'https://www.cannapot.com/shop/hanfsamen/neue-samen/track.php?id=aaa'
    new_url = 'https://www.cannapot.com/shop/hanfsamen/neue-samen/track.php?id=foooo'
    expect(r(old_url)).to eq(new_url)
  end


  # vaposhop.com:
  # append ?a=235

  # normal: https://www.vaposhop.de//vaporizer-brands/focusvape/
  # affiliate: https://www.vaposhop.de//vaporizer-brands/focusvape/?a=235
  it 'changes correctly for vaposhop' do
    SiteSetting.affiliate_vaposhop = '471166642'

    old_url = 'https://www.vaposhop.de//vaporizer-brands/focusvape/'
    new_url = 'https://www.vaposhop.de//vaporizer-brands/focusvape/?a=471166642'
    expect(r(old_url)).to eq(new_url)
  end


  # drehmohment-headshop.de:

  # append ?ref=bt3E2Yv5BAOyLkLrbOGd
  # Any trailing slash at the end of the URL (and before the affiliate parameter) has to be removed 

  # normal: https://www.drehmoment-headshop.de/Vaporizer-Flowermate-V50S-Pro
  # affiliate: https://www.drehmoment-headshop.de/Vaporizer-Flowermate-V50S-Pro?ref=bt3E2Yv5BAOyLkLrbOGd
  it 'changes correctly for drehmohment' do
    SiteSetting.affiliate_drehmohment = '471166642'

    old_url = 'https://www.drehmoment-headshop.de/Vaporizer-Flowermate-V50S-Pro'
    new_url = 'https://www.drehmoment-headshop.de/Vaporizer-Flowermate-V50S-Pro?ref=471166642'
    expect(r(old_url)).to eq(new_url)
  end


  # dutch-passion.com:

  # append ?afmc=2a

  # normal: https://dutch-passion.com/de/hanfsamen/banana-blaze
  # affiliate: https://dutch-passion.com/de/hanfsamen/banana-blaze?afmc=2a
  it 'changes correctly for dutchpassion' do
    SiteSetting.affiliate_dutchpassion = '2a'
    
    old_url = 'https://dutch-passion.com/de/hanfsamen/banana-blaze'
    new_url = 'https://dutch-passion.com/de/hanfsamen/banana-blaze?afmc=2a'
    expect(r(old_url)).to eq(new_url)
  end

  # 
  #   nordicoil.com:
  # append: ?utm_source=pap&utm_medium=affiliate&utm_campaign=logo&utm_term=5c5a90d504e20#a_aid=5c5a90d504e20&a_bid=59323bd0

  # normal: https://nordicoil.de/cbd-hanfoel-5
  # affiliate: https://nordicoil.de/cbd-hanfoel-5?utm_source=pap&utm_medium=affiliate&utm_campaign=logo&utm_term=5c5a90d504e20#a_aid=5c5a90d504e20&a_bid=59323bd0

  it 'changes correctly for nordicoil' do
    SiteSetting.affiliate_nordicoil = '5c5a90d504e20'
    
    old_url = 'https://nordicoil.de/cbd-hanfoel-5'
    new_url = 'https://nordicoil.de/cbd-hanfoel-5?a_aid=5c5a90d504e20&utm_source=pap&utm_medium=affiliate&utm_campaign=logo&utm_term=5c5a90d504e20&a_bid=59323bd0'
    expect(r(old_url)).to eq(new_url)
  end


end
