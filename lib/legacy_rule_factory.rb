# frozen_string_literal: true
#
class LegacyRuleFactory

  def self.create_rules
    rules = {}

  
    rule = lambda do |url, uri|
      code = SiteSetting.affiliate_ldlc_com
      if code.present?
        uri.fragment = code
        uri.to_s
      else
        url
      end
    end

    rules['www.ldlc.com'] = rule
    rules['ldlc.com'] = rule

    #flystein
    rule = lambda do |url, uri|
      code = SiteSetting.affiliate_flystein
      if code.present?
        query_array = [["af", code]]
        uri.query = URI.encode_www_form(query_array)
        uri.to_s
      else
        url
      end
    end

    rules['flystein.com'] = rule
    rules['www.flystein.com'] = rule

    # DEPRRECATED AFFIILIATE SYSTEM
    #sensiseeds + weedseedshop

    #growshop24
    rule = lambda do |url, uri|
      code = SiteSetting.affiliate_growshop24
      if code.present?
        url.chomp!("/")
        uri = url + "?ref=" + code
        uri.to_s
      else
        url
      end
    end

    rules['grow-shop24.de'] = rule
    rules['www.grow-shop24.de'] = rule

    #proemit
    rule = lambda do |url, uri|
      code = SiteSetting.affiliate_proemit
      if code.present?
        url.chomp!("/")
        uri = url + "?sPartner=" + code
        uri.to_s
      else
        url
      end
    end

    rules['pro-emit.de'] = rule
    rules['www.pro-emit.de'] = rule
    rules['shop.pro-emit.de'] = rule

    rules
  end

end
