# frozen_string_literal: true
#
class AmazonRuleFactory


  def self.create_amazon_rule(domain)
    lambda do |url, uri|
      code = SiteSetting.get("affiliate_amazon_#{domain.gsub('.', '_')}")
      if code.present?
        original_query_array = URI.decode_www_form(String(uri.query)).to_h
        query_array = [["tag", code]]
        query_array << ['node', original_query_array['node']] if original_query_array['node'].present?
        uri.query = URI.encode_www_form(query_array)
        uri.to_s
      else
        url
      end
    end
  end

  def self.create_rules
    postfixes = %w{
      com com.au com.br com.mx
      ca cn co.jp co.uk de
      es fr in it nl
    }

    rules = {}

    postfixes.map do |postfix|
      rule = create_amazon_rule(postfix)

      rules["amzn.com"] = rule if postfix == 'com'
      rules["www.amazon.#{postfix}"] = rule
      rules["smile.amazon.#{postfix}"] = rule
      rules["amazon.#{postfix}"] = rule
    end

    rules
  end

end
